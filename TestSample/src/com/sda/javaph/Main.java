package com.sda.javaph;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        // write your code here
        String patternText = "[a-zA-Z]";   // ab,b,aab,aaab......
        String userInput;

        Pattern pattern = Pattern.compile(patternText);
        System.out.println("Please enter text:");
        Scanner scanner = new Scanner(System.in);
        userInput = scanner.next();


        Matcher matcher = pattern.matcher(userInput);

        boolean isMatched = matcher.matches();
        if (isMatched) {
            System.out.println("Correct!");
        } else {
            System.out.println("Invalid format!");
        }

    }


}
