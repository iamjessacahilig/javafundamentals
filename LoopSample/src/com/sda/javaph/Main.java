package com.sda.javaph;

public class Main {

    public static void main(String[] args) {
        // write your code here

        for (int i = 0; i < 10; i++) {
            if (i == 8) {
                continue;
            }
            System.out.println("Hello World!: " + i);
        }
    }
}
