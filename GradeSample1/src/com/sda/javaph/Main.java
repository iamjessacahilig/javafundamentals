package com.sda.javaph;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int grade = 0;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter your grade: ");
        grade = scanner.nextInt();

        if (grade >= 75) {
            System.out.println("Congratulations. You passed!");
        } else {
            System.out.println("Sorry. You failed!");
        }

    }
}
