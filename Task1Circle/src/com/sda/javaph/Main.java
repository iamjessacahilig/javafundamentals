package com.sda.javaph;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        // write your code here

        Scanner scanner = new Scanner(System.in);


        System.out.println("Enter diameter:");
        Circle circle = new Circle();
        circle.setDiameter(scanner.nextDouble());

        //  circle.computePerimeter();
        System.out.println("The value of perimeter is " + circle.computePerimeter());

    }
}
