package com.sda.javaph;

public class Circle {

    private double diameter; // setter
    private double radius; // private no need
    private double perimeter; // getter
    private final double pi = 3.14;

    public void setDiameter(double _diameter) {
        if (_diameter > 0) {
            this.diameter = _diameter;
        } else {
            System.out.println("Invalid input");
        }

    }

/*    public double getPerimeter() {


        return perimeter;
    }*/

    public double computePerimeter() {

        radius = diameter / 2;
        perimeter = (float) (2 * pi * radius);

        return perimeter;
    }
}
