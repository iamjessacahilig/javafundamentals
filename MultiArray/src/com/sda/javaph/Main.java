package com.sda.javaph;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String[][] myArray = new String[2][];
        myArray[0] = new String[]{"R0_C0", "R0_C1", "R0_C2", "R0_C3"}; // creating the first row (index number 0)
        myArray[1] = new String[]{"R1_C0", "R1_C1", "R1_C2", "R1_C3"};


        System.out.println(myArray[0][0]);
        System.out.println(myArray[0][2]);
        System.out.println(myArray[1][1]);
        System.out.println(myArray[1][3]);
        System.out.println(myArray[1][4]);

    }
}
