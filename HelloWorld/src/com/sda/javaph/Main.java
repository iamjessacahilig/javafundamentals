package com.sda.javaph;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int input;
        // 1 = Hello
        // 0 = Bye
        System.out.println("Enter value:");
        input = scanner.nextInt();

        if (input == 1) {
            System.out.println("Hello");

        } else {
            System.out.println("Bye");
        }
       // System.out.println("hello");


       String output= input == 1?"Hello":"Bye";

       System.out.println(output);
       // return          //condition  //if   //else

        // System.out.println(isTrue ? "Hello" : "Bye");
    }
}
