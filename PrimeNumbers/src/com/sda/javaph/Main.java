package com.sda.javaph;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
      
        int range;
        String primeNumbers = "";
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter range:");
        range = scanner.nextInt();

        if (range > 1) {
            for (int x = 1; x < range; x++) {

                if (isPrimeNumber(x)) {
                    primeNumbers = primeNumbers + x + " ";
                }
            }
        } else {
            System.out.println("Invalid range");
        }

        System.out.println(primeNumbers);

    }


    public static boolean isPrimeNumber(int number) {

        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }
}
