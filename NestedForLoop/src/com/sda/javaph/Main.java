package com.sda.javaph;

import java.util.Scanner;

public class Main {
    static final char[] vowels = new char[]{'a', 'e', 'i', 'o', 'u'};

    public static void main(String[] args) {

        String input = "";
        String lowerCaseInput = "";


        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a word:");
        input = scanner.nextLine();
        lowerCaseInput = input.toLowerCase();
        int count = countVowels(lowerCaseInput);
        System.out.println("Vowels: " + count);
    }

    static int countVowels(String input) {
        int count = 0;
        String result = "";
        for (int x = 0; x < input.length(); x++) {
            for (int y = 0; y < vowels.length; y++) {
                if (input.charAt(x) == vowels[y]) {
                    count++;
                    result = input.charAt(x) + ", " + result;
                }
            }

        }

        System.out.println(result);
        return count;

    }
}
















/*    private static void basicLoop() {


        for (int a = 1; a <= 5; a++) { // this loop is for new line - enter
            for (int x = 1; x <= a; x++) { // this loop is for printing *
                System.out.print("*");
            }
            for (int y = 5; y >= a; y--) {
                System.out.print("*");
            }
            System.out.println();
        }
    }*/


