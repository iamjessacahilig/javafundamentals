package com.sda.javaph;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        double a, b, c;
        double x1, x2;
        double delta;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter value of a:");
        a = scanner.nextDouble();

        System.out.print("Enter value of b:");
        b = scanner.nextDouble();

        System.out.print("Enter value of c:");
        c = scanner.nextDouble();

        delta = (b * b) - 4 * a * c;


        if (delta >0) {
            x1 = (-b + Math.sqrt(delta)) / (2 * a);
            x2 = (-b - Math.sqrt(delta)) / (2 * a);
            System.out.println("x1:" + x1);
            System.out.println("x2:" + x2);
        } else {
            System.out.println("Delta negative");
        }
    }
}
