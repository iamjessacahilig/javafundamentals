package com.sda.javaph;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int grade;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter your grade: ");
        grade = scanner.nextInt();

        if (grade >= 86) {
            System.out.println("Very Good!");
        } else  if(grade < 86 && grade>= 75) {
            System.out.println("Good.");
        }else{
            System.out.println("Failed.");
        }

    }
}
