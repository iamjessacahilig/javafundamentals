package com.sda.javaph;

public class Person {
    public String name;
    public String address;
    public String birthdate;
    public int age;


    // a. variable name, variable type
    // b. value, variable name, variable type
    // c. variable type, variable name

    public Person() {
    }

    private void printName() {
    }

    // a. [variable name][parameters]
    // b. [access modifiers][return type][method name][parameters]
    // c. [parameters][method name]

   public void sleep() {

        //
    }

    void read() {
    }

    void walk() {
    }
}
