package com.sda.javaph;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {


    }

    public boolean isValidUrl(String url) {
        String emailRegEx = "(http)://(www\\.)?[a-zA-Z0-9]+\\.(com|net|org)";
        Pattern pattern = Pattern.compile(emailRegEx);
        Matcher matcher = pattern.matcher(url);

        return matcher.matches();
    }
}
