package com.sda.javaph;

public class Main {

    public static void main(String[] args) {

        int array[] = new int[10];

        // this loop will put a value inside int array, and display the value
        for (int x = 0; x < array.length; x++) {
            array[x] = x + 1;
            System.out.println("The value of index[" + x + "] is " + array[x]);
        }


    }


}
