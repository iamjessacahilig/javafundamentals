package com.sda.javaph;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        float firstNumber;
        float secondNumber;
        String operation;

        Scanner scanner = new Scanner(System.in);

        // + - / *
        System.out.print("Enter first number:");
        firstNumber = scanner.nextFloat();

        System.out.print(" + (Addition)\n - (Subtraction) \n / (Division) \n * (Multiplication) \n\n Select operation:");
        operation = scanner.next();

        System.out.print("Enter second number:");
        secondNumber = scanner.nextFloat();

        if (operation.equals("+")) {
            // addition
            System.out.print(firstNumber + secondNumber);
        } else if (operation.equals("-")) {
            // subtraction
            System.out.print(firstNumber - secondNumber);
        } else if (operation.equals("/")) {
            if(secondNumber ==0){
                System.out.println("Cannot Calculate");
            }else{
                System.out.print(firstNumber / secondNumber);
            }
            // divide
        } else if (operation.equals("*")) {
            //multiplication
            System.out.print(firstNumber * secondNumber);
        } else {
            System.out.println("Invalid symbol");
        }

    }
}
