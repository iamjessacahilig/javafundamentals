package com.sda.javaph;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int range;
        String output = "";
        System.out.print("Enter a number:");

        Scanner scanner = new Scanner(System.in);
        range = scanner.nextInt();

        if (range > 1) {

            for (int x = 1; x <= range; x++) {

                if (x % 3 == 0) {
                    output = output + "Fizz ";
                }

                if (x % 7 == 0) {
                    output = output + "Buzz ";
                }

                if (x % 3 != 0 && x % 7 != 0) {
                    output = output + x + " ";
                }


            }
            System.out.println(output);
        } else {
            System.out.println("Range should be greater than 1");
        }

    }
}
